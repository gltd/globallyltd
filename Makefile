
deploy-server:

		@echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
		@echo "DEPLOYING GAME SERVER"
		@echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
		ops/docker-build-and-push-to-ecr.sh
		@echo "service container statuses:"
		ops/ecs-deploy-service.sh

deploy-dev-client:

		@echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
		@echo "DEPLOYING DEV CLIENT"
		cd client && yarn deploy-dev

deploy-prod-client:

		ops/ecs-deploy-service.sh
		@echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
		@echo "DEPLOYING PROD CLIENT"
		cd client && yarn deploy-prod

deploy-all:

		make -s deploy-server
		make -s deploy-dev-client
		make -s deploy-prod-client

format-ops:

		chmod +x ops/*.sh
		chmod 700 ops/*.sh

start-medialive-channel:

		echo "starting medialive channel"
		ops/medalive-start-channel.sh | jq .State

stop-medialive-channel:

		echo "starting medialive channel"
		ops/medalive-stop-channel.sh | jq .State