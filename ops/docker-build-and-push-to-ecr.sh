#!/bin/sh

echo "---------------------------------------------"
echo "logging into ecr"
echo "---------------------------------------------"
`aws --profile gltd ecr get-login --no-include-email`
cd server/
echo "---------------------------------------------"
echo "building docker image"
echo "---------------------------------------------"
docker build -t globally.ltd --build-arg env=production --build-arg port=80 .
echo "---------------------------------------------"
echo "tagging image on ecr"
echo "---------------------------------------------"
docker tag globally.ltd:latest 700950171595.dkr.ecr.us-east-1.amazonaws.com/globally.ltd:latest
echo "---------------------------------------------"
echo "pushing image"
echo "---------------------------------------------"
docker push 700950171595.dkr.ecr.us-east-1.amazonaws.com/globally.ltd:latest
echo "---------------------------------------------"
echo "done!"