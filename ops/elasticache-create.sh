#!/bin/sh

# create the production redis cluster
echo "creating the production redis cluster..."
aws --profile gltd elasticache create-cache-cluster --cache-cluster-id 'globally-ltd-production-redis' --cache-node-type 'cache.r4.large' --cache-subnet-group-name 'globally-ltd-production-cache-subnet' --security-group-ids 'sg-0379105de66c40e80' --engine 'redis' --engine-version '3.2.4' --num-cache-nodes 1 --cache-parameter-group 'default.redis3.2'

# wait until it's available
echo "waiting until the cluster is available..."
aws --profile gltd elasticache wait cache-cluster-available --cache-cluster-id 'globally-ltd-production-redis'

# print the endpoint
echo "here's the cluster's endpoint:"
aws --profile gltd elasticache describe-cache-clusters \
  --cache-cluster-id 'globally-ltd-production-redis' \
  --show-cache-node-inf | jq '.CacheClusters[0].CacheNodes[0].Endpoint'
