import {
    MouseMove,
    TwoFingerScroll,
  } from "./Common/UI/Controls/Icons";

export const CONTENT = {
    "/": {
        artist: "globally.ltd",
        message: "globally.ltd is a virtual clubspace for sound. tonight at 8PM EST we'll be hosting a live performance by J-Words in celebration of her 'Dancepack Vol. 2' on fifteen.pm .",
        purchaseLinkText: "TIP THE ARTIST",
        purchaseLink: "https://venmo.com/jwordss",
        liveStreamVideoId: '82g80JF1U7g',
        colors: {
            logo: '#fff',
            overlay: 'rgba(255, 0, 0, 0.5)',
            overlayContent: '#fff',
            player: '#fff',
            navigation: '#fff',
            onHover: 'rgba(255, 0, 0, 0.5)',
            info: '#fff',
        },
        instructions: [{
            icon: MouseMove,
            text: "click and drag mouse to look around"
        },
        {
            icon: TwoFingerScroll,
            text: "scroll to zoom"
        }]
    },
    "/space-is-the-place": {
        artist: "CLUB FIFTEEN.PM",
        message: "Welcome to the Club.",
        purchaseLinkText: "TIP THE ARTIST",
        purchaseLink: "https://venmo.com/abelsonlive",
        liveStreamVideoId: '82g80JF1U7g',
        colors: {
            logo: '#fff',
            overlay: 'rgba(255, 0, 0, 0.5)',
            overlayContent: '#fff',
            player: '#fff',
            navigation: '#fff',
            onHover: 'rgba(255, 0, 0, 0.5)',
            info: '#fff',
        },
        instructions: [{
            icon: MouseMove,
            text: "click and drag mouse to look around"
        },
        {
            icon: TwoFingerScroll,
            text: "scroll to zoom"
        }]
    }
}