import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route } from 'react-router-dom';
import './index.css';
import Header from './Common/UI/Header';
import history from './history'
import Room_SpaceIsThePlace from "./Rooms/SpaceIsThePlace/index";

ReactDOM.render(
    <Router history={history}>
        <div>
            <Header />
            <Route exact path="/" component={Room_SpaceIsThePlace} />
            <Route path="/space-is-the-place" component={Room_SpaceIsThePlace} />
        </div>
    </Router>,
    document.getElementById('root')
);
module.hot.accept();
