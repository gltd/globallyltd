import React, { useState, useRef, useMemo, useEffect } from 'react';
import { initClient } from "./client";

const RaveContext = React.createContext([{}, () => { }]);

const RaveProvider = ({ name, ...props }) => {
    const [rave, setRave] = useState();
    const client = useRef();

    useEffect(() => {
        client.current = initClient();
    }, [])

    useEffect(() => {
        if (!client.current) return;
        async function initRave() {
            try {
                const r = await client.current.joinOrCreate(name);
                setRave(r)
            }
            catch (error) {
                console.error(error);
            }
        }
        if (!rave) {
            initRave();
        }
    }, [client.current]);


    return <RaveContext.Provider value={{ client, rave, ...props }}>
        {props.children}
    </RaveContext.Provider>
}

export { RaveContext, RaveProvider }