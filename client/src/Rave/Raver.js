import React, { useEffect, useMemo, useContext, useRef } from "react";
import { extend, useThree, useFrame } from 'react-three-fiber';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { useRaver } from './hooks';
import { RaveContext } from './RaveContext';
extend({ OrbitControls })

const Controls = props => {
  const { camera, gl } = useThree()
  const controls = useRef()
  useFrame(() => {
    // TODO not sure if this is the best way to combine orbit controls
    // with keyboard controls, but something like this
    if (props.target) {
      controls.current.target = props.target
    }
    controls.current && controls.current.update();
  })

  return <orbitControls ref={controls} args={[camera, gl.domElement]} {...props} />
}


export default function Raver({ ...props }) {
  const { position } = useRaver()

  return (<>
    <Controls
      enableDamping
      rotateSpeed={0.3}
      dampingFactor={0.1}
      enableKeys={false}
      target={position}
    />
  </>);
}
