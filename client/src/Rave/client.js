import { Client } from "colyseus.js";

export function initClient() {
  // setup game server url based on environment
  const nodeENV = process.env.NODE_ENV || "local";
  console.log("[gltd-client] running environment: ", process.env.NODE_ENV);

  const PROTOCOL = window.location.protocol.replace("http", "ws");
  const HOST_NAME = window.location.hostname;

  let gameServerURL = "ws://localhost:2657";
  if (HOST_NAME == "globally.ltd" || HOST_NAME == "dev.globally.ltd") {
    gameServerURL = `${PROTOCOL}//globally-ltd-game-server.herokuapp.com/`;
    // gameServerURL = `${PROTOCOL}//game.globally.ltd/`;
  } else if (nodeENV == "local-docker") {
    gameServerURL = "ws://localhost:2657";
  } else if (nodeENV == "development") {
    gameServerURL = "ws://localhost:2657";
  }
  console.log("[gltd-client] connecting to game server: ", gameServerURL );
  return new Client(gameServerURL);
}