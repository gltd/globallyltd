export const namespacedRoom = (name) => { 
  const hostname = window.location.hostname;
  // namesmpaed dev server
  if (hostname == "dev.globally.ltd") {
    return name + '-dev';
  } else {
    return name;
  }
};